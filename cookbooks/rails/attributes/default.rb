
default["rails"]["user"]    = "vagrant"
default["rails"]["group"]   = "vagrant"
default["rails"]["owner"]   = "vagrant"
default["rails"]["version"] = "4.2.2"
