#
# Cookbook Name:: rails
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "install rails" do
  user node["rails"]["user"]
  group node["rails"]["group"]
  environment "HOME" => "/home/#{node["rails"]["user"]}"
  code "/home/#{node["rails"]["user"]}/.rbenv/shims/gem install --no-ri --no-rdoc rails -v #{node["rails"]["version"]}"
  creates "/home/#{node["rails"]["user"]}/.rbenv/shims/rails"
end

directory "/home/vagrant/workspace/" do
  owner node["rails"]["user"]
  user node["rails"]["user"]
  group node["rails"]["group"]
  action :create
end
