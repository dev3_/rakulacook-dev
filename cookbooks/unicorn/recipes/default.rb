#
# Cookbook Name:: unicorn
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

gem_package "unicorn" do
    gem_binary "/home/#{node["unicorn"]["user"]}/.rbenv/shims/gem"
    version node["unicorn"]["version"]
    action :install
end

directory "/home/vagrant/unicorn/" do
  owner node["unicorn"]["owner"]
  user node["unicorn"]["user"]
  group node["unicorn"]["group"]
  action :create
end

directory "/home/vagrant/unicorn/log/" do
  owner node["unicorn"]["owner"]
  user node["unicorn"]["user"]
  group node["unicorn"]["group"]
  action :create
end

template "unicorn.service" do
  path "/etc/systemd/system/unicorn.service"
  source "unicorn.service.erb"
end

template ".bash_profile" do
  path "/home/#{node["unicorn"]["user"]}/.bash_profile"
  source ".bash_profile.erb"
  owner node["unicorn"]["user"]
  group node["unicorn"]["group"]
  mode 0644
end