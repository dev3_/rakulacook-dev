
default["unicorn"]["user"]    = "vagrant"
default["unicorn"]["group"]   = "vagrant"
default["unicorn"]["owner"]   = "vagrant"
default["unicorn"]["version"] = "5.1.0"
default["unicorn"]["path"] = "/home/#{node["unicorn"]["user"]}/unicorn"
default["unicorn"]["log_path"] = "#{node["unicorn"]["path"]}/log"
default["unicorn"]["pid_path"] = node["unicorn"]["path"]
default["unicorn"]["sock_path"] = node["unicorn"]["path"]
